# Spring JWT Auth Demo

This is a demo example for implementing JWT Authentication in Java Spring Boot. This project aim to make a base template for implementing JWT Authentication in Spring Boot Project. This demo will also provide you with some custom examples like Custom User, Custom Authority, Custom Security Filter Chain, and many more. I made 2 profiles that you can select in application.properties, dynamic and fixed. The difference between these two is dynamic will used auto generated RSA Key when it runs and fixed will use custom RSA Key from certs folder inside the resource folder. With dynamic profile you don't need to set your own RSA Key and everytime the application lauch, the key will be different from the previous run. With fixed profile, you need to provide your own RSA key file. 

##### Dynamic Profile Advantages:
You don't need to provide your own RSA Key, it will be auto-generated whenever you start the project.

##### Fixed Profile Advatages:
You can use the same RSA Key with different project or microservice that have the same JWT Auth mechanics, so the token that you are generating from this project can be also used with other project as long as the RSA Key is same and have the same JWT Auth mechanics or vice versa.

## Used Library
This project used some library like:
* Lombok (For Reducing boilter plate code like getter, setter, constructor)
* ModelMapper (For mapping DTO into Entity objects)
* H2 Database (In-memory database for testing)
* Gson (For serializing and deserializing object to json)

## Requirements

You need to have at least Java 17 to run this project and Maven installed on your computer. This project use Spring Boot 3.

## Getting started

You can start by clone this repo to your local computer or download it as zip.
Before running the project, you can adjust which profile (dynamic or fixed) do you want to use by set the spring.profiles.active in appplication.properties.

Run the project simply by trigger maven command like this
```
mvn spring-boot:run
```
