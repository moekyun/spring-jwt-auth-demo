package com.studiobidji.springexamplesjwtauthentication;

import com.studiobidji.springexamplesjwtauthentication.user.entity.UserInfoEntity;
import com.studiobidji.springexamplesjwtauthentication.user.repository.UserInfoRepository;
import com.studiobidji.springexamplesjwtauthentication.utils.CustomAuthorityUtil.CustomAuthorityEnum;
import com.studiobidji.springexamplesjwtauthentication.utils.CustomAuthorityUtil;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.HashSet;
import java.util.Set;

//@EnableConfigurationProperties(StaticRsaKeyConfigProperties.class)
@SpringBootApplication
public class SpringExamplesJwtAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringExamplesJwtAuthenticationApplication.class, args);
	}

	@Bean
	public CommandLineRunner initializeUser(UserInfoRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
		return args -> {

			UserInfoEntity user = new UserInfoEntity();
			user.setUsername("exampleuser");
			user.setEmail("example@gmail.com");
			user.setPassword(passwordEncoder.encode("password"));
			user.setFirstName("Example User");
			user.setLastName("Nani");

			Set<CustomAuthorityEnum> roles = new HashSet<>();
			roles.add(CustomAuthorityEnum.ADMIN);
			roles.add(CustomAuthorityEnum.USER);

			user.setAuthority(CustomAuthorityUtil.enumSetToString(roles));

			// Save the user to the database
			userRepository.save(user);

		};
	}
}
