package com.studiobidji.springexamplesjwtauthentication.user.service;

import com.studiobidji.springexamplesjwtauthentication.user.dto.UserInfoDto;
import com.studiobidji.springexamplesjwtauthentication.user.entity.UserInfoEntity;
import com.studiobidji.springexamplesjwtauthentication.user.repository.UserInfoRepository;
import com.studiobidji.springexamplesjwtauthentication.utils.CustomAuthorityUtil;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserInfoService{

    //Init the logger
    private final Logger logger = LoggerFactory.getLogger(getClass());

    ModelMapper modelMapper = new ModelMapper();

    UserInfoRepository userInfoRepository;

    public UserInfoService(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

    public UserInfoEntity registerNewUser(UserInfoDto user){

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);

        if(!CustomAuthorityUtil.validateAuthority(user.getAuthority())){
            throw new IllegalArgumentException("Invalid Role Detected!");
        }

        UserInfoEntity targetNewUser = modelMapper.map(user, UserInfoEntity.class);

        return userInfoRepository.save(targetNewUser);
    }
}
