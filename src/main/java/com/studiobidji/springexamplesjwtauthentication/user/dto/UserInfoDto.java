package com.studiobidji.springexamplesjwtauthentication.user.dto;

import lombok.Data;

@Data
public class UserInfoDto {

    private Long id;

    private String username;

    private String email;

    private String password;

    private String firstName;

    private String lastName;

    private String authority;

}
