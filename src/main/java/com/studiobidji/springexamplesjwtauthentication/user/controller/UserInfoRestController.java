package com.studiobidji.springexamplesjwtauthentication.user.controller;

import com.studiobidji.springexamplesjwtauthentication.user.dto.UserInfoDto;
import com.studiobidji.springexamplesjwtauthentication.user.entity.UserInfoEntity;
import com.studiobidji.springexamplesjwtauthentication.user.service.UserInfoService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
public class UserInfoRestController {

    private final UserInfoService userInfoService;

    public UserInfoRestController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    @PostMapping("/register")
    public UserInfoEntity processRegister(@RequestBody UserInfoDto user) {
        try {
            return userInfoService.registerNewUser(user);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }

    }

}
