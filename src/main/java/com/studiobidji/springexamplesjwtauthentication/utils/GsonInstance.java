package com.studiobidji.springexamplesjwtauthentication.utils;

import com.nimbusds.jose.shaded.gson.Gson;
import com.nimbusds.jose.shaded.gson.GsonBuilder;

public class GsonInstance {

    public static final GsonBuilder builder = new GsonBuilder();
    public static final Gson gson = builder.setPrettyPrinting()
            .create();

}
