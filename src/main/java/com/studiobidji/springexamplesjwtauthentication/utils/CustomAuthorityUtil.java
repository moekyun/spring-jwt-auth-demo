package com.studiobidji.springexamplesjwtauthentication.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class CustomAuthorityUtil {

    //Init the logger
    private static final Logger logger = LoggerFactory.getLogger(CustomAuthorityUtil.class);

    public static enum CustomAuthorityEnum {
        ADMIN,
        USER
    }
    public static String enumSetToString(Set<? extends Enum<?>> enumSet) {

        String delimiter = ";";

        // Check if the set is not null
        if (enumSet == null) {
            return null; // or throw an exception, depending on your requirements
        }

        // Use StringBuilder to efficiently concatenate the enum names
        StringBuilder result = new StringBuilder();

        // Iterate over the set elements
        for (Enum<?> enumValue : enumSet) {
            // Append the enum name and delimiter to the StringBuilder
            result.append(enumValue.name()).append(delimiter);
        }

        // Remove the trailing delimiter, if any
        if (result.isEmpty()) {
            result.deleteCharAt(result.length() - 1);
        }

        // Convert StringBuilder to String
        return result.toString();
    }

    public static boolean validateAuthority(String authInput){

        String delimiter = ";";

        String[] authorities = authInput.split(delimiter);

        logger.info("Current authorities = " + String.join(", ", authorities));

        for(String authority : authorities){
            boolean validRole = false;
            for (CustomAuthorityEnum authEnum : CustomAuthorityEnum.values()) {
                if (authEnum.name().equalsIgnoreCase(authority)) {
                    validRole = true;
                    break;
                }
            }
            if (!validRole) {
                return false;
            }
        }

        return true;
    }
}
