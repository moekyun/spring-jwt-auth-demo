package com.studiobidji.springexamplesjwtauthentication.debug.dto;

public record DebugDto(String name, int age) {

}