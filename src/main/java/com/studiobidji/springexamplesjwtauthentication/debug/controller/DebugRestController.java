package com.studiobidji.springexamplesjwtauthentication.debug.controller;

import com.studiobidji.springexamplesjwtauthentication.debug.dto.DebugDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/debug")
public class DebugRestController {

    //Init the logger
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("/test")
    public DebugDto testDebugAppRunning(){

        //Debugging Variables
        int debugInt = 27;
        String debugDesc = "Hello World - Spring JWT Examples";

        //Creating New DebugDto
        DebugDto returnData = new DebugDto(debugDesc, debugInt);
        logger.info("Debug test api called = {}", returnData );

        //Return the test Data
        return returnData;
    }

    @GetMapping("/admin")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public DebugDto getDebugDataAdmin(){

        //Debugging Variables
        int debugInt = 21;
        String debugDesc = "Test Admin Data - Spring JWT Examples";

        //Creating New DebugDto
        DebugDto returnData = new DebugDto(debugDesc, debugInt);
        logger.info("Debug test api called = {}", returnData );

        //Return the test Data
        return returnData;
    }

    @GetMapping("/user")
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    public DebugDto getDebugDataUser(){

        //Debugging Variables
        int debugInt = 17;
        String debugDesc = "Test User Data - Spring JWT Examples";

        //Creating New DebugDto
        DebugDto returnData = new DebugDto(debugDesc, debugInt);
        logger.info("Debug test api called = {}", returnData );

        //Return the test Data
        return returnData;
    }

}
