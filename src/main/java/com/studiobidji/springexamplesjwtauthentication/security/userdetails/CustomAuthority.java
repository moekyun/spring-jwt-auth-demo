package com.studiobidji.springexamplesjwtauthentication.security.userdetails;

import com.studiobidji.springexamplesjwtauthentication.utils.CustomAuthorityUtil;
import org.springframework.security.core.GrantedAuthority;

public class CustomAuthority implements GrantedAuthority {

    private final CustomAuthorityUtil.CustomAuthorityEnum authority;

    public CustomAuthority(CustomAuthorityUtil.CustomAuthorityEnum role) {
        this.authority = role;
    }

    @Override
    public String getAuthority() {
        return authority.toString();
    }
}