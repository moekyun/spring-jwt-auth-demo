package com.studiobidji.springexamplesjwtauthentication.security.userdetails;

import com.studiobidji.springexamplesjwtauthentication.user.entity.UserInfoEntity;
import com.studiobidji.springexamplesjwtauthentication.user.repository.UserInfoRepository;
import com.studiobidji.springexamplesjwtauthentication.utils.CustomAuthorityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfoEntity userInfo = userInfoRepository
                .findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User name not found: " + username));

        String[] roleArray = userInfo.getAuthority().split(";");

        Set<CustomAuthority> authorities =
                Arrays.stream(roleArray)
                        .map(role ->
                                new CustomAuthority(
                                        CustomAuthorityUtil.CustomAuthorityEnum.valueOf(role)
                                ))
                        .collect(Collectors.toSet());

        return new CustomUserDetails(userInfo, authorities);
    }
}