package com.studiobidji.springexamplesjwtauthentication.security.controller;

import com.studiobidji.springexamplesjwtauthentication.security.dto.JwtAuthRequestDto;
import com.studiobidji.springexamplesjwtauthentication.security.dto.JwtAuthResponseDto;
import com.studiobidji.springexamplesjwtauthentication.security.service.AuthService;
import com.studiobidji.springexamplesjwtauthentication.security.userdetails.CustomUserDetails;
import com.studiobidji.springexamplesjwtauthentication.utils.GsonInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthRestController {

    //Init the logger
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final AuthService authService;

    private final AuthenticationManager authenticationManager;

    public AuthRestController(AuthService authService, AuthenticationManager authenticationManager) {
        this.authService = authService;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping
    public JwtAuthResponseDto authenticate(@RequestBody JwtAuthRequestDto jwtTokenRequestDto){
        try {
            logger.info("request = " + GsonInstance.gson.toJson(jwtTokenRequestDto) );

            Authentication authentication =
                    authenticationManager
                            .authenticate(new UsernamePasswordAuthenticationToken(
                                    jwtTokenRequestDto.username(),
                                    jwtTokenRequestDto.password()));
            SecurityContextHolder.getContext().setAuthentication(authentication);

            logger.info("authentication = " + GsonInstance.gson.toJson(authentication.getAuthorities()));

            CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();

            return new JwtAuthResponseDto(authService.createToken(authentication));
        }catch (Exception ex){
            logger.error("exception = {0}", ex);
            throw new RuntimeException(ex);
        }
    }

}

