package com.studiobidji.springexamplesjwtauthentication.security.config.fixed;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@ConfigurationProperties(prefix = "rsa")
public record StaticRsaKeyConfigProperties(RSAPublicKey publicKey, RSAPrivateKey privateKey) {

}
