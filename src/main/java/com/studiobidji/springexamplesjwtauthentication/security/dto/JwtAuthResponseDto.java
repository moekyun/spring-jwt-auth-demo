package com.studiobidji.springexamplesjwtauthentication.security.dto;

public record JwtAuthResponseDto(String token) {
}
