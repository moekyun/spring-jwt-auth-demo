package com.studiobidji.springexamplesjwtauthentication.security.dto;

public record JwtAuthRequestDto(String username, String password) {}
